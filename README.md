# Cups

**Group Members and ID Numbers:**

*  Britney Beckford (1702381) - **Group Leader**
*  Kamaaria Nixon (1702413) - **Integration Lead**

**Description of the project**

Cups is a local coffee shop that provides a relaxing getaway in the middle of the city for the disabled community. They are also a wonderful example of a Social Enterprise Boost Initiative (https://www.micaf.gov.jm/msme-initiatives/social-enterprise-boost-initiative-sebi) similar to DeafCan coffee (https://www.deafcancoffee.com/ | https://youtu.be/gGYq0ASoEPM ). Kat, the manager, has been encouraged by her mentor to establish another store at 95 Moolean Avenue in the heart of Montego Bay. Kat would like to encourage an empowering environment through self service. Your consulting team providing pro bono services has considered to incorporate Artificial Intelligence through Computer Vision and Speech Processing to accomplish this. The touch-screen self service kiosk will allow customers to order their favourite treats and verify using their Digital Id.

[Click Here for Erd and Project Charter](https://gitlab.com/BritneyB/cups/blob/master/ERD%20and%20Wireframes.pdf)
Please see ERD and Wireframes document for project charter